# di18n - localization utilities for Dojo

In short this project provides support for plural forms and a dijit mixin for simplifying localization in Dojo.

## Module 'di18n/localize' - variable substitution and plural forms
The module provides a single method that allows localization strings with variable substitution.
A variable can either look like $n where n is between 1 and 9 OR ${var} where 'var' can be any alphanumeric name 
of a variable. The module also support choosing the right plural form in a plural expressions containing a variable.
The choice is made by treating the value to be substituted for the variable as an integer and using a language specific
rule (determined by which localization string that was found) to choose which plural form to use in the plural expression.
A plural expression can look like:

    There are {{PLURAL:$1|one cat|$1 cats}} in the picture. 
   
The module depends on 'di18n/pluralRules' and that the bundle with localization strings where loaded via the 
'di18n/i18n' plugin rather than the default 'dojo/i18n' plugin.

See the API documentation in 'di18n/localize' for more details.

## Module 'di18n/NLSMixin' - simplified construction of localized widgets
The module provides class that is suitable for mixing with 'dijit/_WidgetBase' and provides:

1. declaration mechanism in template for connecting nodes to localization keys
2. five i18n states to allow switching between different localization keys in same position
3. dependencies between properties and localization keys (e.g. for variable substitution and plural forms)
4. re-localization when:
    1. dependent variables are changed via the set method from _WidgetBase
    2. language are changed via setLocale in 'di18n/locale' module
    3. i18n state are changed via the 'setI18nState' method (state 0-4)
5. the 'localeChange' method - an extentionpoint where specific localization code can be written

There are three attributes in NLSMixin which are crucial for the behaviour:

Attribute | Value 
---- | ----- 
nlsBundles | array of strings, each corresponding to a bundle
nlsBundleBase | base path for the bundle strings in nls, e.g. 'di18n/samples/nls/'
nlsParameters | optional map of dependant parameters per localization key 

In the template the localization is triggered by an expression like the following:
<div data-dojo-attach-point="nls">key1</div>

There are two things to notice here:

1. the attachpoint 'nls' used to notify that this node is i18n controlled
2. the name of the localization key, in this case it is named 'key1'

See the API documentation in 'di18n/NLSMixin' for more details for how to use the mixin.

## Example use
1. See the 'di18n/samples/Localize.html' for how to use the 'di18n/localize' module.
2. See the 'di18n/samples/NLSMixinWidget.html' for how to use the 'di18n/NLSMixin' module.

## Relationship to dojo
This project builds on top of dojos libraries and provides alternative localization utilities than what 
is provided by dojo.  All files are written from scratch except the di18n/i18n.js file which is in large 
a copy of dojo/i18n.js. The only difference is a modification that avoids merging together keys in different languages 
for the same bundle.