/*global define,__confolio*/
define([
    "dojo/_base/declare",
    "dojo/_base/lang",
    "dojo/on",
    "dojo/dom-attr",
    "dijit/_Widget",
    "dijit/_TemplatedMixin",
    "di18n/NLSMixin",
    "dojo/text!./NLSMixinWidgetTemplate.html"
], function(declare, lang, on, domAttr, _Widget, _TemplatedMixin, NLSMixin, template) {

    return  declare([_Widget, _TemplatedMixin, NLSMixin], {
        nlsBundleBase: "di18n/samples/nls/",
        nlsBundles: ["messages"],
        nlsParameters: {
            "chair": "nr",
            "troll": ["nr"],
            "friendly": {first: "givenName", last: "familyName"}
        },

        templateString: template,
        nr: 0,
        givenName: "John",
        familyName: "Doe",

        postCreate: function() {
            this.inherited("postCreate", arguments);
            this.initNLS();
            on(this._button, "click", lang.hitch(this, function() {
                var value = this.get("nr")+1;
                this.set("nr", value);
            }))
        }
    });
});