define({
    friendly: "Välkommen ${first} ${last}",
    troll: "{{PLURAL|ett troll|$1 troll}}",
    chair: "{{PLURAL|en stol|$1 stolar}}"
})