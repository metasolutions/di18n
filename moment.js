define(["moment", "di18n/locale"], function(moment, locale) {
    locale.useMoment(moment);
    return moment;
});