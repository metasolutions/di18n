/*global define,__confolio*/
define([
    "dojo/_base/kernel",
    "dojo/_base/declare",
    "dojo/_base/lang",
    "dojo/Deferred",
    "dojo/topic",
    "dojo/_base/array",
    "dojo/dom-attr",
    "dijit/_WidgetBase",
    "di18n/i18n",
    "./localize"
], function(kernel, declare, lang, Deferred, topic, array, domAttr, _WidgetBase, i18n, localize) {

    var globalNlsBundleBase;

    var NLSMixin = declare(null, {
        /**
         * Array of localization bundles to load.
         * Provide this if you need localization support!
         */
        nlsBundles: [],

        /**
         * Optional base to which all nls will be resolved, e.g. "folio/nls/".
         */
        nlsBundleBase: "",

        /**
         * Optional configuration of which localization keys that depend upon other parameter(s).
         * Note that when parameters change (via this.set("paramname", newvalue) localization values in
         * the template will update automatically due to this explicit dependency map.
         *
         * The map should contain the localization key as key and the value can be either:
         * - a single named parameter (the same name or $1 can be used in the localization string)
         * - a range of parameters (the name or the position in the array can be used in the localization string)
         * - a map of parameters (the key in the map should be used in the localization string)
         *
         * For instance, the following two expressions provides the same dependant parameters for the i18n key 'key1':
         *  - nlsParameters: {"key1": "counter"}  //Access via $1, ${1} or ${counter} in localization string.
         *  - nlsParameters: {"key1": ["counter"]} //Access via $1, ${1} or ${counter} in localization string.
         *
         * The last alternative, to use a map of the dependant parameters, is useful if you want to make
         * the localization string easier to read by providing good names for the variables in the localization string.
         * For instance this is useful when the names of the parameters in the dijit-class are not good enough (e.g. to long.)
         * For instance:
         *  - nlsParameters: {"key1": {"nr": "counter"}} //Only access via ${nr}, not via ${counter} or ${1}
         *
         * Example 1: a friendly greeting:
         *  - nlsParameters: {"greeting": {"first": "givenname", "last": "familyname"}}   //In Dijit
         *  - "greeting": "Welcome ${first} ${last}!"                                          //In english bundle
         *
         * Example 2: plurals support:
         *  - nlsParameters: {"howmanycats": "counter"}                            //In Dijit
         *  - "howmanycats": "There are {{PLURAL:$1|one cat|$1 cats}} in the picture."  //In english bundle
         */
        nlsParameters: {},

        /**
         * Loaded localization bundles. E.g. if a bundle is called 'test' you can access it via:
         * this.NLSBundles.test
         */
        NLSBundles: {},

        /**
         * Array of attachpoints those specified in template will end up in this array. Initial values
         * (like the innerHTML) of these attachpoints will be treated as keys to be looked up in the
         * localization bundles.
         */
        nls: [],

        /**
         * A promise that will be invoked after the nlsbundles has been loaded for the first time.
         * @type {dojo/promise/Promise}
         */
        localeReady: null,

        /**
         * If multiple keys are given as values in the template (as innerHTML9 for the i18n attachpoints the key used
         * will depend on the current i18nState. If only one key is provided that will be used independently of the state.
         * Note, do not set manually, use 'setI18nState' method instead.
         */
        _i18nState: 0,
        _i18nKeys: [],
        _i18nKeyMap: {},
        _i18nAttributes: ["innerHTML", "title", "placeholder"],

        constructor: function() {
            this.nls = [];
            this.NLSBundles = {};
            this.localeReady = new Deferred();
        },

        setI18nState: function(state) {
            this._i18nState = state;
            this._localeChange();
        },
        localeChange: function() {
        },

        /**
         * Call this method from postCreate.
         */
        initNLS: function() {
            //Normalize and create inverse map to simplify parameter check
            var properties = {}, normalized = {};
            for (var key in this.nlsParameters) if (this.nlsParameters.hasOwnProperty(key)) {
                var val = this.nlsParameters[key], obj = {};
                if (lang.isString(val)) {
                    obj["1"] = val;
                    obj[val] = val;
                    properties[val] = true;
                } else if (lang.isArray(val)) {
                    array.forEach(val, function(v, idx) {
                        obj[""+(idx+1)] = v;
                        obj[v] = v;
                        properties[v] = true;
                    })
                } else if (lang.isObject(val)) {
                    obj = val;
                    for (var key2 in val) if (val.hasOwnProperty(key2)) {
                        properties[val[key2]] = true;
                    }
                }
                normalized[key] = obj;
            }
            this.nlsParameters = normalized;
            //Checking (without creating a dependency) if subclass of dojo/Stateful.
            if (this.watch && this.get && this.set) {
                //If so, listen for changes to variables that can trigger a locale change, e.g. for plurals.
                this.watch(lang.hitch(this, function (name, oldValue, value) {
                    if (properties.hasOwnProperty(name)) {
                        //Lazy implementation, localizing all keys rather than those specifically concerned.
                        this._localeChange();
                    }
                }));
            }
            //Connect to application events
            this._localeChangeSubcription = topic.subscribe("/di18n/localeChange",
                lang.hitch(this, this._localeChange));

            this._i18nKeys = [{}, {}, {}, {}, {}]; //Maximum four states.
            var f = lang.hitch(this, function(keys, node, attr) {
                var key;
                keys = keys.split(/\s*,\s*/);
                if (keys.length === 1) {
                    key = lang.trim(keys[0]);
                    this._addToOrCreateArr(this._i18nKeys[0], key, node);
                    this._i18nKeyMap[key] = attr;
                } else {
                    for (var state=0;state<keys.length;state++) {
                        key = lang.trim(keys[state]);
                        this._addToOrCreateArr(this._i18nKeys[state+1], key, node);
                        this._i18nKeyMap[key] = attr;
                    }
                }
            });
            array.forEach(this.nls, function(node) {
                if (node instanceof _WidgetBase) {
                    f(node.get("label"), node, dijit);
                } else {
                    array.forEach(this._i18nAttributes, function(attr) {
                        if (attr !== "innerHTML" || (node.childNodes.length === 1
                            && node.childNodes[0].nodeType === 3)) {
                            f(domAttr.get(node, attr) || "", node, attr);
                        }
                    });
                }
            }, this);
            this._localeChange();
        },

        destroy: function() {
            if (this._localeChangeSubcription) {
                this._localeChangeSubcription.remove();
                delete this._localeChangeSubcription;
            }
            if (this.inherited) {
                this.inherited(arguments);
            }
        },

        _addToOrCreateArr: function(obj, key, node) {
            if (obj[key]) {
                obj[key].push(node);
            } else {
                obj[key] = [node];
            }
        },
        _localizeKey: function(bundle, key) {
            if (this.nlsParameters.hasOwnProperty(key)) {
                var params = {}, map = this.nlsParameters[key];
                for (var k in map) if (map.hasOwnProperty(k)) {
                    params[k] = this.get(map[k]);
                }
                return localize(bundle, key, params);
            } else {
                return bundle[key];
            }
        },
        _localizeKeys: function(bundle, keys2nodes) {
            var showKey = kernel.locale === "nls";
            for (var key in keys2nodes) if (bundle[key] && keys2nodes.hasOwnProperty(key)) {
                array.forEach(keys2nodes[key], function(node) {
                    if (node instanceof _WidgetBase) {
                        node.set("label", showKey ? key : this._localizeKey(bundle, key));
                    } else {
                        domAttr.set(node, this._i18nKeyMap[key], showKey ? key : this._localizeKey(bundle, key));
                    }
                }, this);
            }
        },
        _localeChange: function() {
            var counter = this.nlsBundles.length;
            array.forEach(this.nlsBundles, function(bundleName, idx) {
                i18n.getLocalization(this.nlsBundleBase || globalNlsBundleBase, bundleName, kernel.locale,
                    lang.hitch(this, function(bundle) {
                        this["NLSBundle"+idx] = bundle;
                        this.NLSBundles[bundleName] = bundle;
                        this._localizeKeys(bundle, this._i18nKeys[0]);
                        if (this._i18nState !== 0) {
                            this._localizeKeys(bundle, this._i18nKeys[this._i18nState]);
                        }
                        counter--;
                        if (counter === 0) {
                            if (!this.localeReady.isResolved()) {
                                this.localeReady.resolve();
                            }
                            this.localeChange.call(this, arguments);
                        }
                    }));
            }, this);
        }
    });

    NLSMixin.setGlobalNlsBundleBase = function(base) {
        globalNlsBundleBase = base;
    };

    NLSMixin.Dijit = declare([NLSMixin], {
        postCreate: function () {
            this.inherited("postCreate", arguments);
            this.initNLS();
        }
    });

    return NLSMixin;
});
