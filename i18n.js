/**
 * @license RequireJS i18n 2.0.5 Copyright (c) 2010-2012, The Dojo Foundation All Rights Reserved.
 * Available via the MIT or new BSD license.
 * see: http://github.com/requirejs/i18n for details
 */
/*jslint regexp: true */
/*global require: false, navigator: false, define: false */

/**
 * This plugin handles i18n! prefixed modules. It does the following:
 *
 * 1) A regular module can have a dependency on an i18n bundle, but the regular
 * module does not want to specify what locale to load. So it just specifies
 * the top-level bundle, like "i18n!nls/colors".
 *
 * This plugin will load the i18n bundle at nls/colors, see that it is a root/master
 * bundle since it does not have a locale in its name. It will then try to find
 * the best match locale available in that master bundle, then request all the
 * locale pieces for that best match locale. For instance, if the locale is "en-us",
 * then the plugin will ask for the "en-us", "en" and "root" bundles to be loaded
 * (but only if they are specified on the master bundle).
 *
 * Once all the bundles for the locale pieces load, then it mixes in all those
 * locale pieces into each other, then finally sets the context.defined value
 * for the nls/colors bundle to be that mixed in locale.
 *
 * 2) A regular module specifies a specific locale to load. For instance,
 * i18n!nls/fr-fr/colors. In this case, the plugin needs to load the master bundle
 * first, at nls/colors, then figure out what the best match locale is for fr-fr,
 * since maybe only fr or just root is defined for that locale. Once that best
 * fit is found, all of its locale pieces need to have their bundles loaded.
 *
 * Once all the bundles for the locale pieces load, then it mixes in all those
 * locale pieces into each other, then finally sets the context.defined value
 * for the nls/fr-fr/colors bundle to be that mixed in locale.
 */
(function () {
    'use strict';

    //regexp for reconstructing the master bundle name from parts of the regexp match
    //nlsRegExp.exec("foo/bar/baz/nls/en-ca/foo") gives:
    //["foo/bar/baz/nls/en-ca/foo", "foo/bar/baz/nls/", "/", "/", "en-ca", "foo"]
    //nlsRegExp.exec("foo/bar/baz/nls/foo") gives:
    //["foo/bar/baz/nls/foo", "foo/bar/baz/nls/", "/", "/", "foo", ""]
    //so, if match[5] is blank, it means this is the top bundle definition.
    var nlsRegExp = /(^.*(^|\/)nls(\/|$))([^\/]*)\/?([^\/]*)/;

    //Helper method to normalize a name with help of the regexp above.
    var normalize = function(name) {
        var match = nlsRegExp.exec(name),
            norm = {prefix: match[1]};

        //If match[5] is blank, it means this is the top bundle definition,
        //so it does not have to be handled. Locale-specific requests
        //will have a match[4] and a match[5]
        if (match[5]) {
            //locale-specific bundle
            norm.locale = match[4];
            norm.suffix = match[5];
            norm.masterName = norm.prefix + norm.suffix;
        } else {
            //Top-level bundle.
            norm.suffix = match[4];
            norm.masterName = name;
        }
        return norm;
    };

    //Helper function to avoid repeating code. Lots of arguments in the
    //desire to stay functional and support RequireJS contexts without having
    //to know about the RequireJS contexts.
    function addPart(locale, master, needed, toLoad, prefix, suffix) {
        if (master[locale]) {
            needed.push(locale);
            if (master[locale] === true || master[locale] === 1) {
                toLoad.push(prefix + locale + '/' + suffix);
            }
        }
    }

    function addIfExists(req, locale, toLoad, prefix, suffix) {
        var fullName = prefix + locale + '/' + suffix;
        if (require._fileExists(req.toUrl(fullName + '.js'))) {
            toLoad.push(fullName);
        }
    }

    /**
     * Utility method to create prototype hierarchy between objects.
     */
    var inherit = function(proto) {
        function F() {}
        F.prototype = proto;
        return new F
    };

    /**
     * Simple function to mix in properties from source into target,
     * but only if target does not already have a property of the same name.
     * This is not robust in IE for transferring methods that match
     * Object.prototype names, but the uses of mixin here seem unlikely to
     * trigger a problem related to that.
     */
    function mixin(target, source, force) {
        var prop;
        for (prop in source) {
            if (source.hasOwnProperty(prop) && (!target.hasOwnProperty(prop) || force)) {
                target[prop] = source[prop];
            } else if (typeof source[prop] === 'object') {
                if (!target[prop] && source[prop]) {
                    target[prop] = {};
                }
                mixin(target[prop], source[prop], force);
            }
        }
    }

    define(['module', 'require'], function (module, require) {
        var masterConfig = (module.config ? module.config() : {}) || {};
        var overrides = {};
        var overloadPromise = null;
        var _req = require;
        var loadForLocale = function (norm, onLoad) {
            //First, fetch the master bundle, it knows what locales are available.

            var f1 = function (master) {
                //Figure out the best fit
                var needed = [],
                    current = "",
                    value = {},
                    toLoad = [],
                    part, i,
                    parts = norm.locale.split("-");

                //Always allow for root, then do the rest of the locale parts.
                addPart("root", master, needed, toLoad, norm.prefix, norm.suffix);
                for (i = 0; i < parts.length; i++) {
                    part = parts[i];
                    current += (current ? "-" : "") + part;
                    addPart(current, master, needed, toLoad, norm.prefix, norm.suffix);
                }

                var f2 = function () {
                    var i, partBundle, part;
                    for (i = 0;i<needed.length && needed[i]; i++) {
                        part = needed[i];
                        partBundle = master[part];
                        if (partBundle === true || partBundle === 1) {
                            partBundle = _req(norm.prefix + part + '/' + norm.suffix);
                        }
                        value = inherit(value);
                        value.$locale = part;
                        mixin(value, partBundle);
                        //If any overrides are registered for specific locale
                        var override = overrides[norm.masterName];
                        if (override && override[part]) {
                          value = inherit(value);
                          value.$locale = part;
                          mixin(value, override[part]);
                        }
                    }

                    //All done, notify the loader.
                    if (onLoad) {
                        onLoad(value);
                    } else {
                        return value;
                    }
                };

                //Load all the parts missing.
                if (onLoad) {
                    _req(toLoad, f2);
                } else {
                    return f2();
                }
            };

          if (onLoad) {
            if (overloadPromise != null) {
              overloadPromise.then(function () {
                _req([norm.masterName], f1);
              });
            } else {
              _req([norm.masterName], f1);
            }
          } else {
            var master = _req(norm.masterName);
            return f1(master);
          }
        };

        var loadForLocaleDeps = function (norm, onLoad) {
            _req([norm.masterName],function (master) {
                //Figure out the best fit
                var needed = [], current = "", toLoad = [],
                    part, i, parts = norm.locale.split("-");
                addPart("root", master, needed, toLoad, norm.prefix, norm.suffix);
                for (i = 0; i < parts.length; i++) {
                    part = parts[i];
                    current += (current ? "-" : "") + part;
                    addPart(current, master, needed, toLoad, norm.prefix, norm.suffix);
                }
                _req(toLoad, onLoad);
            });
        };


        var explicitSetLocale;
        var getCurrentLocale = function() {
            if (explicitSetLocale) {
                return explicitSetLocale;
            }
            if (!masterConfig.locale) {
                masterConfig.locale = typeof navigator === "undefined" ? "root" :
                    ((navigator.languages && navigator.languages[0]) ||
                    navigator.language ||
                    navigator.userLanguage || "root").toLowerCase();
            }
            return masterConfig.locale;
        };


        var allNLSDeps = [], exp;

        return exp = {
            version: '2.0.5',
            /**
             * Called when a dependency needs to be loaded.
             */
            load: function (name, req, onLoad, config) {
                exp.addNLSDeps([name]);
                var norm = normalize(name);
//                _req = req;
                config = config || {};

                if (config.locale) {
                    masterConfig.locale = config.locale;
                }
                if (!norm.locale) {
                    norm.locale = getCurrentLocale();
                }

                if (config.isBuild) {
                    var parts = norm.locale.split("-"),
                        toLoad = [],
                        i, part, current = "";

                    //Check for existence of all locale possible files and
                    //require them if exist.
                    toLoad.push(norm.masterName);
                    addIfExists(req, "root", toLoad, norm.prefix, norm.suffix);
                    for (i = 0; i < parts.length; i++) {
                        part = parts[i];
                        current += (current ? "-" : "") + part;
                        addIfExists(req, current, toLoad, norm.prefix, norm.suffix);
                    }

                    //If several languages are explicitly listed in config
                    if (config.extraLocale) {
                        for (i=0;i<config.extraLocale.length;i++) {
                            addIfExists(req, config.extraLocale[i], toLoad, norm.prefix, norm.suffix);
                        }
                    }

                    req(toLoad, function () {
                        onLoad();
                    });
                } else {
                    loadForLocale(norm, onLoad);
                }
            },
            getLocalization: function(moduleName, bundleName, locale, onLoad) {
                //Backward compatability for dojo's old dot-notation in package names.
                moduleName = moduleName.replace(/\./g, "/");

                //---- Start AMDClean case.
                if (eval("typeof "+moduleName.replace(/\//g, "_")+"_nls_"
                    +bundleName+" !== \"undefined\"")) {
                    return eval(moduleName.replace(/(\.|\/)/g, "_")+"_nls_"+bundleName).root;
                }
                //---- End AMDClean case

                moduleName += moduleName[moduleName.length-1] !== "/" ? "/":"";
                if (moduleName.indexOf("nls") === -1) {
                    moduleName += "nls/";
                }
                var norm = normalize(moduleName+bundleName);
                norm.locale = locale || getCurrentLocale();
                return loadForLocale(norm, onLoad);
            },
            addNLSDeps: function(deps) {
                for (var i=0;i<deps.length;i++) {
                    if (allNLSDeps.indexOf(deps[i]) === -1) {
                        allNLSDeps.push(deps[i]);
                    }
                }
            },
            requireLocalizationDependencies: function(onLoad) {
                if (allNLSDeps == null || allNLSDeps.length === 0) {
                    onLoad();
                    return;
                }
                var counter = allNLSDeps.length;
                for (var i=0;i<allNLSDeps.length;i++) {
                    var norm = normalize(allNLSDeps[i]);
                    norm.locale =  getCurrentLocale();
                    loadForLocaleDeps(norm, function() {
                        counter--;
                        if (counter === 0) {
                            onLoad();
                        }
                    });
                }
            },
            normalizeLocale: function(locale) {
                return locale ? locale.toLowerCase() : getCurrentLocale();
            },
            setCurrentDefaultLocale: function(locale) {
                explicitSetLocale = locale;
            },
            /**
             * @param name - the name of the nls-bundle, e.g. "nls/listcomponent"
             * @param bundle - the nls strings divided into the their locale sub-bundles
             * where "root" is the default,
             * e.g. { root: {key1: "some value}, sv: {key1: "något värde"}}.
             */
            addOverride: function(name, bundle) {
              overrides[name] = bundle;
            },
            setOverridesPath: function(path) {
              overloadPromise = new Promise((resolve, reject) => {
                _req([path], function(bundles) {
                  overrides = bundles;
                  overloadPromise = null;
                  resolve();
                });
              });
            }
        };
    });
}());
