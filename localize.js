/*global define,__confolio*/
define([
    "dojo/_base/lang",
    "dojo/_base/array",
    "dojo/string",
    "./pluralRules"
], function(lang, array, string, pluralRules) {

    var getPrototypeOf = (function() {
        if ( typeof Object.getPrototypeOf !== "function" ) {
            if ( typeof "test".__proto__ === "object" ) {
                return function(object){
                    return object.__proto__;
                };
            } else {
                return function(object){
                    // May break if the constructor has been tampered with
                    return object.constructor.prototype;
                };
            }
        } else {
            return Object.getPrototypeOf;
        }
    })();

    var findLocaleOfKey = function(bundle, key) {
        if (bundle.hasOwnProperty(key)) {
            if (bundle.hasOwnProperty("$locale")) {
                return bundle["$locale"];
            }
        }
        var prot = getPrototypeOf(bundle);
        if (prot != null) {
            return findLocaleOfKey(prot, key);
        }
    };

    var normalizeParameters = function(parameters) {
        if (typeof parameters === "string") {
            return {"1": parameters};
        } else if (parameters === parseInt(parameters, 10)) {
            return {"1": parameters};
        } else if (lang.isArray(parameters)) {
            var obj = {};
            for (var i=0;i<parameters.length;i++) {
                obj[""+i] = parameters[i];
            }
            return obj;
        } else if (lang.isObject(parameters)) {
            return parameters;
        }
        throw "Parameters must be either an integer, an array or an object.";
    };

    var splitLocalizedValue = function(str) {
        var cstr = str.replace(/\$(\d)/g, "\${$1}");
        var arr = cstr.split(/({{|}})/g);
        arr = array.filter(arr, function(item) {
            return item !== "{{" && item != "}}";
        });
        return array.map(arr, function(item, idx) {
            if (idx % 2 == 1) {
                try {
                    var obj = {};
                    var plarr = item.split("|");
                    var conf = plarr[0].split(":");
                    obj.type = conf[0];
                    if (conf.length === 2) {
                        obj.variable = conf[1].replace(/\${(.*)}/, "$1");
                    } else {
                        obj.variable = "1";
                    }
                    plarr.shift();
                    obj.forms = plarr;
                    return obj;
                } catch (e) {
                    throw "Syntax error in localization string: "+str;
                }
            } else {
                return item;
            }
        });
    };

    var joinLocalizedValue = function(arr, language, parameters) {
        var rule = pluralRules.findRule(language);
        var larr = array.map(arr, function(item) {
            if (lang.isObject(item)) {
                var nr = parameters[item.variable];
                nr = parseInt(nr);
                return item.forms[rule(nr)];
            } else {
                return item;
            }
        });
        return larr.join("");
    };


    /**
     * Localizes the key by looking it up in the bundle and replace any variables with values
     * provided in third parameter (named parameters). If no third argument is provided the
     * function does nothing more than returning the value of bundle[key].
     *
     * The strength of this function is that it handles the third argument 'parameters'
     * quite cleverly. The third parameter is allowed to be a single value, an array or a map (object).
     * - single value: replace any occurence of variable $1 (or ${1})
     * - array of values: replaces occurences of variables $1, $2, etc with corresponding values in the array.
     * - map: replaces all variables that appear as keys in the map with their corresponding values.
     *
     * In addition, if the localization string contains a plural constructions like:
     * "There are {{PLURAL:$1|one cat|$1 cats}} in the picture."
     * then the correct plural form will be detected. The detection mechanism relies on:
     * (1) the language for the localization string yielding a specific plural form rule, see module pluralRules and note 1 below.
     * (2) the value to be substituted for the variable ($1 in the above example) will be assumed to be an integer
     * and used as input to the plural form rule from (1).
     *
     * Note 1: the language for the localization string will be detected by checking which of the bundles the
     * string originated from in the bundle hierarchy. This requires that localization bundles for different languages
     * are ordered into a hierarchy via the prototype. E.g. en if current language is 'sv-SE' it's prototype will be a
     * bundle for 'sv' and it's prototype in turn is the default bundle (typically in english).
     * Creating such a prototype hierarchy of localization bundles is unfortunately not the default behaviour of the
     * 'dojo/i18n' plugin, hence for this to work the bundle must be loaded with the 'di18n/i18n' plugin instead.
     *
     * @param {Object} bundle is the localization bundle for the current language.
     * @param {String} key for which a localized value is requested.
     * @param {integer|array|object} parameters for localizing the string.
     */
    return function(bundle, key, parameters) {
        var val = bundle[key];
        if (val == null) {
            throw "No value for key: "+key;
        }
        var language = findLocaleOfKey(bundle, key);
        if (parameters == null) {
            return bundle[key];
        }
        var par = normalizeParameters(parameters);
        var arr = splitLocalizedValue(val);
        var str = joinLocalizedValue(arr, language, par);
        try {
            return string.substitute(str, par);
        } catch(err) {
            console.warn("Could not replace all variables in string: \""+str+"\".");
            return str;
        }
    };
});
