/*global define,__confolio*/
define([
    "require",
    "dojo/_base/kernel",
    "dojo/_base/connect",
    "dojo/_base/array",
    "di18n/i18n"
], function(require, kernel, connect, array, i18n) {

    var allNLSDeps = [], moment, momentPath, momentMap = {
        "no": "nb"
    };
    i18n.setCurrentDefaultLocale(kernel.locale);
    var l = {
        get: function() {
            return kernel.locale;
        },
        loadMomentLocale: function(locale, callback) {
            if (locale === "en") { //English is built in into the core moment, no need to load.
                moment.locale(locale);
                callback && callback();
            } else {
                locale = l.getMomentLocale(locale);
                require(["moment/locale/"+locale], function() {
                    moment.locale(locale);
                    callback && callback();
                });
            }
        },
        useMoment: function(m) {
            if (typeof moment === "undefined") {
                moment = m;
                if (l.get()) {
                    l.loadMomentLocale(l.get());
                }
            }
        },
        setMomentLocaleMap: function(map) {
            momentMap = map;
        },
        getMomentLocale: function(locale) {
            return momentMap[locale] || locale;
        },
        getCurrentMomentLocale: function() {
            return l.getMomentLocale(l.get());
        },
        set: function(locale, onLocaleChange) {
            var oldlocale = kernel.locale;
            kernel.locale = locale;
            i18n.setCurrentDefaultLocale(locale);
            i18n.requireLocalizationDependencies(function() {
                var f = function() {
                    connect.publish("/di18n/localeChange", [locale, oldlocale]);
                    onLocaleChange && onLocaleChange();
                };
                if (typeof moment !== "undefined") {
                    l.loadMomentLocale(locale, f);
                } else {
                    f();
                }
            });
        },
        addNLSDeps: function(deps) {
            allNLSDeps = allNLSDeps.concat(deps);
            deps = array.map(deps, function(dep) {return "di18n/i18n!"+dep});
            require(deps, function() {});
        }
    };
    return l;
});